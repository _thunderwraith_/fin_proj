$(document).ready(function () {
  $(".slider").slick({
    prevArrow: "<i class='fa fa-angle-left arrow prev' aria-hidden='true'></i>",
    nextArrow: "<i class='fa fa-angle-right arrow next' aria-hidden='true'></i>"
  });

  var parentItems = document.querySelector(".stars");
  var allItems = document.querySelectorAll(".stars .star");
  var activeItems = document.querySelectorAll(".stars .star.active").length;

  var cStars = function (nowPos) {
    for (var i = 0; allItems.length > i; i++) {
      allItems[i].classList.remove("active");
    }
    for (var i = 0; nowPos + 1 > i; i++) {
      allItems[i].classList.toggle("active");
    }
  };

  parentItems.addEventListener("mouseover", function (e) {
    var myTarget = e.target;
    var i = allItems.length;
    while (i--) {
      if (allItems[i] == myTarget) {
        var currentIndex = i;
        break;
      }
    }
    cStars(currentIndex);
  });

  parentItems.addEventListener("click", function (e) {
    var myTarget = e.target;
    var i = allItems.length;
    while (i--) {
      if (allItems[i] == myTarget) {
        var currentIndex = i;
        break;
      }
    }
    cStars(currentIndex);
    activeItems = document.querySelectorAll(".stars .star.active").length;
  });

  parentItems.addEventListener("mouseleave", function (e) {
    cStars(+activeItems - 1);
  });

  $(".js-tab-trigger").click(function () {
    let id = $(this).attr("data-tab")
    let content = $('.js-tab-content[data-tab="' + id + '"]');

    $(".js-tab-trigger.active").removeClass("active");
    $(this).addClass("active");

    $(".js-tab-content.active").removeClass("active");
    content.addClass("active");
  });

  $(".lookbook").click(function () {
    $(".lookbook").toggleClass("clicked--lookbook");
  });

  $(".minus").click(function () {
    var $input = $(this)
      .parent()
      .find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $(".plus").click(function () {
    var $input = $(this)
      .parent()
      .find("input");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });

  $(".js-form").each(function (index, form) {
    var messagesString = $(form).attr("data-messages");
    var parsedMessages = JSON.parse(messagesString);
    $(form).validate({
      messages: parsedMessages
    });
  });
});
